import { RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';
import { MessagesService } from './messages/messages.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { MessagesComponent } from './messages/messages.component';
import { MessagesFormComponent } from './messages/messages-form/messages-form.component';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { NavigationComponent } from './navigation/navigation.component';
import { UsersComponent } from './users/users.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { MessageComponent } from './messages/message/message.component';
import { NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { UsersService } from './users/users.service';
import { UsersFormComponent } from './users/users-form/users-form.component';
import { UpdateFormComponent } from './messages/update-form/update-form.component';
import { LoginComponent } from './login/login.component';
import { UserComponent } from './users/user/user.component';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { environment } from './../environments/environment';
import { MessagesfComponent } from './messagesf/messagesf.component';




@NgModule({
  declarations: [
    AppComponent,
    MessagesComponent,
    MessagesFormComponent,
    NavigationComponent,
    UsersComponent,
    NotFoundComponent,
    MessageComponent,
    UsersFormComponent,
    UpdateFormComponent,
    LoginComponent,
    UserComponent,
    MessagesfComponent,
   
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(environment.firebase), 
    AngularFireDatabaseModule,
    RouterModule.forRoot([
      {path:'',component:MessagesComponent},//ריק כי מציין את דף הבית
      {path:'users',component:UsersComponent},
      {path:'message/:id', component:MessageComponent},
      {path:'user/:id', component:UserComponent},
      {path:'updateForm/:id', component:UpdateFormComponent},
      {path:'messagesf', component:MessagesfComponent},
      {path:'login', component:LoginComponent},
      {path:'**',component:NotFoundComponent}//חשוב שהוא יהיה אחרון כי זה אומר - כל השאר... כל מה שלא הגדרנו ילך ל"לא נמצא"
      
    ])

  ],
  providers: [
    MessagesService,
    UsersService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }