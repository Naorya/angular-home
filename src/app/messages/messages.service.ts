import { Injectable } from '@angular/core';
import { Http,Headers } from '@angular/http';
import { HttpParams } from '@angular/common/http';
import 'rxjs/Rx';//בשביל האופרטור 'מאפ'      ח
import { AngularFireDatabase } from 'angularfire2/database';

@Injectable()
export class MessagesService {
   http:Http;// http-> שם התכונה, Http-> סוג התכונה
  
   getMessagesFire(){
    
      return this.db.list('/messages').valueChanges();
      }
    
   login(credentials){// זה הסרבר ששולח לשרת את הנתונים של הלוגין לבדיקה
    let options = { // הגדרנו דרך המחלקה של אנגולר האדר , רכיב קונטנט טייפ בישביל שהנתונים יעברו
    headers:new Headers({
     'content-type':'application/x-www-form-urlencoded'
   })
 }
let  params = new HttpParams().append('user', credentials.user).append('password',credentials.password); //  מתכוננים לשליחת הנתונים ,פאראמס הוא מבנה נתונים המחזיק מפתח וערך 
return this.http.post('http://localhost/slim/auth', params.toString(),options).map(response=>{ // שליחה לראוט אוט
  let token = response.json().token; // המשתנה טוקן מכיל את הג'יידטי שהגיע מהשרת
  if (token) localStorage.setItem('token',token); // שמירה של הטוקן בלוקל סטורג'
  console.log(token);


});
}

getMessages(){
  //return['Message1','Message2','Message3','Message4'];
  //get messages from the SLIM rest API(dont say DB)
  let token = localStorage.getItem('token');
  let options = {//מוסיפים את הטוקן להדר של הבקשה
    headers: new Headers({
      'Authorization':'Bearer '+token
    })
  }  
  return this.http.get('http://localhost/slim/messages',options);

  }
  
  
  
  getMessage(id){
     return this.http.get('http://localhost/slim/messages/'+id);//מאשפשר לנו למשוך את ההודעה 
  }
  
  postMessage(data){//השיטה תקבל קובץ גייסון ותחליף אותו
    let options =  {
      headers:new Headers({//הגדרנו דרך מחלקה מיוחדת של אנגולר  שנקראת הדרס שליחה של קי וואליו
        'content-type':'application/x-www-form-urlencoded'
      })
    }
    let params = new HttpParams().append('message',data.message);//פאראמס הוא למעשה מבנה נתונים שמחזיק קי ו- ואליו, קי הוא המאסג' והואליו הו א הדאטא.מסאג
     return this.http.post('http://localhost/slim/messages',params.toString(),options);
  }
  putMessage(data,key){
    let options = {
      headers: new Headers({
        'content-type':'application/x-www-form-urlencoded'
      })
    }
    let params = new HttpParams().append('message',data.message);
    return this.http.put('http://localhost/slim/messages/'+ key,params.toString(), options);
  }

 deleteMessage(key){
    return this.http.delete('http://localhost/slim/messages/'+key);
 }
  constructor(http:Http, private db:AngularFireDatabase) { //נוצר האובייקט מסוג אייץ טי טי פי
    this.http = http; 
  }
}