import { MessagesService } from './../messages.service';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup , FormControl } from '@angular/forms';

@Component({
  selector: 'messagesForm',//השם של אלמנט האי טי מל החדש שאנחנו יוצרים -הקומפוננט
  templateUrl: './messages-form.component.html',
  styleUrls: ['./messages-form.component.css']
})
export class MessagesFormComponent implements OnInit {

  @Output() addMessage:EventEmitter <any> = new EventEmitter <any>(); // -מגדירים את שם המשתנה בשם אווטפוט מסוג איונט אמיטר כאשר מבצעים השמה של אמיטר חדש ובכך בנינו תשתית של העברת מידע מהבן שהוא מסאג' פורם לאב שהוא מסאגס
  @Output() addMessagePs:EventEmitter <any> = new EventEmitter <any>(); //pasemistic event


  service:MessagesService;
  msgform = new FormGroup({//בנייה של מבנה נתונים בקוד שמתאים לטופס
      message:new FormControl(""),
      user:new FormControl(""),
  });

  sendData(){
  this.addMessage.emit(this.msgform.value.message); //ברגע שהיוזר לחץ על סנד תשלח למסאגס הודעה שאומרת לו תציג- זאת אומרת תשתמש באווטפוט שהגדרנו למעלה,האירוע שהתקיים זה האג מסאגז
  console.log(this.msgform.value);//לוקח את הערכים שהזנתי בפורם ושולח אותם לקונסול. עם אפ12
  this.service.postMessage(this.msgform.value).subscribe(//שמירה ב דאטא בייס
    response =>{
      console.log(response.json())
      this.addMessagePs.emit();//רקמ מתזמן את האירוע בניגוד לאופטימי שמעדכן מידע
    }

  )
}
  constructor(service:MessagesService) {
    this.service = service;

   }

  ngOnInit() {
  }

}