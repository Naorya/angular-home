import { Component, OnInit , Output , EventEmitter} from '@angular/core';
import { MessagesService } from './../messages.service';
import {FormGroup , FormControl} from '@angular/forms';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'updateForm',
  templateUrl: './update-form.component.html',
  styleUrls: ['./update-form.component.css']
})
export class UpdateFormComponent implements OnInit {
//בניית ערוץ התקשורת בין message form to message
@Output() addMessage:EventEmitter<any> = new EventEmitter<any>(); //any מגדיר סוג מסוים של מידע, במקרה הזה כל סוג
@Output() addMessagePs:EventEmitter<any> = new EventEmitter<any>(); //אירוע פסימי

service:MessagesService;
//Reactive Form
//מתאים בדיוק לטופס עצמו
msgform = new FormGroup({
    message:new FormControl(),
    user:new FormControl()
});

constructor(private route: ActivatedRoute ,service: MessagesService) {
  this.service = service;
 }
//שליחת הנתונים
sendData() {
  //הפליטה של העדכון לאב
this.addMessage.emit(this.msgform.value.message);
console.log(this.msgform.value);
this.route.paramMap.subscribe(params=>{
  let id = params.get('id');
  this.service.putMessage(this.msgform.value, id).subscribe(
    response => {
      console.log(response.json());
      this.addMessagePs.emit();
    }
  );
})
}

 message;
ngOnInit() {
  this.route.paramMap.subscribe(params=>{
    let id = params.get('id');
    console.log(id);
    this.service.getMessage(id).subscribe(response=>{
      this.message = response.json();
      console.log(this.message);
    })
})
}

}