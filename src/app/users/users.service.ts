import { Injectable } from '@angular/core';
import { Http,Headers } from '@angular/http';
import { HttpParams } from '@angular/common/http';


@Injectable()
export class UsersService {
  http:Http;
  
  postUser(data){//השיטה תקבל קובץ גייסון ותחליף אותו
    let options =  {
      headers:new Headers({//הגדרנו דרך מחלקה מיוחדת של אנגולר  שנקראת הדרס שליחה של קי וואליו
        'content-type':'application/x-www-form-urlencoded'
      })
    }
    let params = new HttpParams().append('user',data.user);//פאראמס הוא למעשה מבנה נתונים שמחזיק קי ו- ואליו, קי הוא המאסג' והואליו הו א הדאטא.מסאג
     return this.http.post('http://localhost/slim/users',params.toString(),options);
  }
  getUser(id){
    return  this.http.get('http://localhost/slim/users/'+id);
  }

  deleteUser (key){
    return this.http.delete('http://localhost/slim/users/'+key)
  }

  getUsers(){
    //return ['a','b','c'];
    //get users from the SLIM rest API (Don't say DB)
    return  this.http.get('http://localhost/slim/users');
  }
  
  constructor(http:Http) { 
    this.http = http;
  }
}